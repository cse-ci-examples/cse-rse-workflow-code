# Computational Science and Engineering (CSE) Research Software Engineering (RSE) Workflow 

This repository implements a Minimal Working Example (MWE) of a research software, used to describe the CSE-RSE workflow. The MWE is a simple C++ application that evaluates derivatives of a given polynomial using Finite Differences and compares the results with exact values. The research data is stored as a single CSV file.

# Publications 

Maric, T. ( 2022-07-21 ). Computational Science and Engineering - Research Software Engineering Workflow : Research Report . Technical University of Darmstadt . https://doi.org/10.48328/tudatalib-921.2

* Research Data: Maric, T. ( 2022-07-07 ). Computational Science and Engineering - Research Software Engineering Workflow : Research Data . Technical University of Darmstadt . https://doi.org/10.48328/tudatalib-910.2

* Repository snapshot:  Maric, T. ( 2022-07-07 ). Computational Science and Engineering - Research Software Engineering Workflow : Research Code . Technical University of Darmstadt . https://doi.org/10.48328/tudatalib-911.5

# Installation  

## Prerequisites 

* CMake (Version 3.16)

## Installation steps

```
    cse-rse-workflow-code > mkdir build && cd build 
    build > cmake .. && make
```

# Running 

```
    build > ./myapp 
```

generates research data `build/poly-data.csv`.  

# Visualizaiton 

Start

```
    cse-rse-workflow-code > jupyter notebook 
```

open `visualize-data.ipynb` and re-run the notebook.
